(function () {

  'use strict'

  /* posting to google sheets plus validation logic:
      https://script.google.com/home/projects/1HMHjkbjE_4s_DXrUgQhuyMpH-mQM3Uo9kLINqJOS7Ikm2Nug3jRh7IXQ/edit
      https://docs.google.com/spreadsheets/d/1lnmcTi-AXRbgSSvl_yf1GIq3MnOirJ8bJB_0FkU4juw/edit?resourcekey#gid=1136682784
      https://github.com/jamiewilson/form-to-google-sheets
  */

  const forms = document.querySelectorAll('form');
  const fields = document.querySelectorAll('input, select, textarea');

  fields.forEach(field => {

    field.customUISet = {
      containerElement: document.getElementById(`${field.id}-container`),
      messageElement: document.getElementById(`${field.id}-validation-message`)
    }

    field.addEventListener('input', () => {

      let isFieldValid = field.validity.valid;

      if(isFieldValid) {
        delete field.form.customUISet.invalidFields[field.id];
        field.customUISet.containerElement.classList.remove("error");
        field.customUISet.messageElement.innerText = '';
      }

      if(Object.keys(field.form.customUISet.invalidFields).length == 0) {
        field.form.classList.remove("error");
      }
    });

    field.addEventListener('focusout', () => {
      field.checkValidity();
    });

    field.addEventListener('invalid', e => {
      field.form.customUISet.invalidFields[field.id] = 'error';
      field.form.classList.add("error");
      field.customUISet.containerElement.classList.add("error");
      field.customUISet.messageElement.innerText = field.validationMessage;
    });

  });

  forms.forEach(form => {

    form.customUISet = {
      invalidFields: {},
      submitBtn: document.querySelectorAll(`#${form.id} button`),
      submitBtnText: document.getElementById(`${form.id}-btn-text`),
      submitSpinWrap: document.getElementById(`${form.id}-spin-wrap`),
      submitSpinCheck: document.getElementById(`${form.id}-spin-check`)
    }

    form.addEventListener('submit', e => {

      let isFormValid = form.reportValidity();

      let key = e.target.dataset['key'];
      let scriptURL = `https://script.google.com/macros/s/${key}/exec`;

      let thisForm = form;

      e.preventDefault();

      if(isFormValid) {

        form.customUISet.submitSpinWrap.classList.add(`fast-fade-in`);

        fetch(scriptURL, {
          method: 'POST',
          body: new FormData(e.target)
        })
        .then(response => {

          form.customUISet.submitSpinCheck.classList.remove("spinner");
          form.customUISet.submitSpinCheck.classList.add("check");

        })
        .catch(error => {

          form.customUISet.submitSpinWrap.classList.remove("fast-fade-in");
          form.customUISet.submitSpinWrap.classList.add("fast-fade-out");

          console.error('Error!', error.message);

        })
      }
    })
  });

  /* Google Maps code */

  function initMap() {

    const upwLatLng = { lat:49.281870 , lng: -123.108160 };

    const map = new google.maps.Map(document.getElementById("map"), {
      center: upwLatLng,
      zoom: 15,
      disableDefaultUI: true,
      mapId: '230f6c5b4285003c'
    });

    const svgIcon = {
      url: "/assets/images/map-point.svg",
      size: new google.maps.Size(30, 60),
      scale: 1,
      anchor: new google.maps.Point(15, 60)
    }

    new google.maps.Marker({
      position: upwLatLng,
      map: map,
      title: "Upword Digital Industries",
      icon: svgIcon
    });
  }

  window.initMap = initMap;

  const wait = ms => new Promise(resolve => setTimeout(resolve, ms));

})();